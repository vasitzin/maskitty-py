## What is this

This is a utility I originally wrote in [C++](https://gitlab.com/vasitzin/maskitty) and now ported to Python for educational purposes.
It calculates the affinity mask for a cpu core range.

## How to use it

You'll need to pass the `-m` flag to provide the core range eg `0-3,6`,
this will return the affinity mask for cores `0, 1, 2, 3 and 6` which is `4F`.

```
~❱ python maskitty-py -m=0-3,6
4F 
```

There's also a help menu

```
~❱ python maskitty-py.py -h
maskitty-py v1.0.0

-m=/--mask= [RANGE]  the range of nums representing your affinity mask (eg 0-3,6)
-t/--threads         return number of threads in output eg 'maskitty-py.py -m=0-5 -t' returns '6 3F'
-h/--help            print this menu
```
