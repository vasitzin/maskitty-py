import sys

TITLE = 'maskitty-py'
VERSION = '1.0.0'

# Expand a number range of N-M format.
def expand(splits):
    res = []
    for s in splits:
        found = s.find('-')
        if found != -1:
            start = int(s[:found])
            stop = int(s[found + 1:])
            for num in range(start, stop + 1):
                res.append(num)
        else:
            res.append(int(s))
    return res


# Parse cmd options string, get value.
def get_cmd_option(args, soption, loption):
    cmd = ''
    for v in args:
        if (v.find(soption) != -1 or v.find(loption) != -1):
            eq = v.find('=')
            cmd = v[eq + 1:]
            return cmd
    return cmd


# Parse cmd options string, get bool of existance.
def is_cmd_option(args, soption, loption):
    for v in args:
        if (v.find(soption) != -1 or v.find(loption) != -1):
            return True
    return False

# Calculate the codes for the cores specified.
def calc_codes(cores):
    codes = []
    prev = 1
    codes.append(prev)
    for c in range(0, cores):
        prev = 2 * prev
        codes.append(prev)
    return codes

if is_cmd_option(sys.argv, '-h', '--help'):
    print(TITLE + ' v' + VERSION + '\n')
    print('-m=/--mask= [RANGE]\tthe range of nums representing your affinity mask (eg 0-3,6)')
    print('-t/--threads\t\treturn number of threads in output eg "maskitty -m=0-5 -t" returns "6 3F"')
    print('-h/--help\t\tprint this menu')

mask_arg = get_cmd_option(sys.argv, '-m=', '--mask=')
mask_range = expand(mask_arg.split(','))
codes = calc_codes(max(mask_range))

mask_dec = 0
for i in mask_range:
    mask_dec += codes[i]

if is_cmd_option(sys.argv, '-t', '--threads'):
    print('{} '.format(len(mask_range)), end = '')

print(str(hex(mask_dec))[2:].upper())
